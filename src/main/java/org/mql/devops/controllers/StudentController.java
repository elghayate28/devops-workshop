package org.mql.devops.controllers;

import java.util.List;

import org.mql.devops.business.UniversityService;
import org.mql.devops.models.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class StudentController {

	@Autowired
	private UniversityService universityService;



	@GetMapping("/get-students")
	public String getAllStudents(Model model) {
		List<Student> students = universityService.students();
		model.addAttribute("students", students);
		return "print-students";
	}
	
	@GetMapping("/insert")
	public String getFormInsertStudent(Model model) {
		model.addAttribute("student",new Student());
		return "insert-student";
	}
	
	

	@GetMapping("/delete")
	public String getFromDeletetStudent(Model model) {
		model.addAttribute("student",new Student());
		return "delete-student";
	}
	@GetMapping("/update")
	public String getFromUpdateStudent( @RequestParam("cnie") String cnie,Model model) {
		model.addAttribute("student",new Student());
		model.addAttribute("cnie", cnie);
		return "update-student";
	}

	@PostMapping("/save-student")
	public String InsertStudentInList(Student student,Model model) {
		if (student.getCnie() == null || student.getCnie().isEmpty()) {
		}
		universityService.insertStudent(student);
		
		return getAllStudents(model);
	}
	
	@PostMapping("/delete-student")
	public String deleteStudent(String cnie, Model model) {
		if (cnie == null || cnie.isEmpty()) {
			model.addAttribute("message", "CNIE is required for deletion");
			return "delete-student";
		}
		Student student = universityService.deleteStudentByCnie(cnie);
		if (student != null) {
			model.addAttribute("message", "Student with CNIE " + cnie + " deleted successfully.");
		} else {
			model.addAttribute("message", "Student with CNIE " + cnie + " not found.");
		}
		List<Student> students = universityService.students();
		model.addAttribute("students", students);
		return "print-students";
	}
	




}
