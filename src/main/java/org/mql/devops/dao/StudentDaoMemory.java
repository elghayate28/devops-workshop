package org.mql.devops.dao;

import java.util.List;

import org.mql.devops.models.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
@Configuration
public class StudentDaoMemory implements StudentDao{
	
	@Autowired
	private List<Student> students;

	public List<Student> selectAll() {
		return students;
	}

	@Override
	public boolean save(Student student) {
		return students.add(student);
	}

	@Override
	public Student deleteById(String cnie) {
		for (Student student : students) {
			if(student.getCnie().equals(cnie)) {
				students.remove(student);
				return student;
			}
		}
		return null;
	}


}
