CREATE DATABASE IF NOT EXISTS university;
USE university;

DROP TABLE IF EXISTS students;
CREATE TABLE students (
    cnie varchar(11) NOT NULL,
    firstname varchar(30) DEFAULT NULL,
    lastname varchar(30) DEFAULT NULL,
    course varchar(255) DEFAULT NULL,
    PRIMARY KEY (cnie)
);

INSERT INTO students (cnie, firstname, lastname, course) VALUES
('12345678901', 'John', 'Doe', 'Computer Science'),
('23456789012', 'Jane', 'Smith', 'Mathematics'),
('34567890123', 'Alice', 'Johnson', 'Physics'),
('45678901234', 'Bob', 'Williams', 'Chemistry'),
('56789012345', 'Charlie', 'Brown', 'Biology'),
('67890123456', 'David', 'Jones', 'History'),
('78901234567', 'Eve', 'Davis', 'Literature'),
('89012345678', 'Frank', 'Miller', 'Philosophy'),
('90123456789', 'Grace', 'Wilson', 'Engineering'),
('01234567890', 'Hannah', 'Moore', 'Art'),
('10987654321', 'Ivy', 'Taylor', 'Economics'),
('19876543210', 'Jack', 'Anderson', 'Political Science'),
('28765432109', 'Kathy', 'Thomas', 'Sociology'),
('37654321098', 'Leo', 'Jackson', 'Anthropology'),
('46543210987', 'Mia', 'White', 'Psychology'),
('55432109876', 'Nina', 'Harris', 'Geography'),
('64321098765', 'Owen', 'Martin', 'Environmental Science'),
('73210987654', 'Paula', 'Thompson', 'Law'),
('82109876543', 'Quinn', 'Garcia', 'Medicine'),
('91098765432', 'Rita', 'Martinez', 'Music'),
('01987654322', 'Sam', 'Robinson', 'Architecture'),
('29876543211', 'Tina', 'Clark', 'Education'),
('38765432110', 'Uma', 'Rodriguez', 'Business Administration'),
('47654321099', 'Vince', 'Lewis', 'Marketing'),
('56543210988', 'Wendy', 'Lee', 'Journalism'),
('65432109877', 'Xander', 'Walker', 'Graphic Design'),
('74321098766', 'Yara', 'Hall', 'Nursing'),
('83210987655', 'Zane', 'Allen', 'Statistics'),
('92109876544', 'Amy', 'Young', 'International Relations'),
('01234567891', 'Ben', 'King', 'Theology');